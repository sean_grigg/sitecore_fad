﻿using Microsoft.Practices.ServiceLocation;
using SitecoreFindADoctorSearch.Models;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Sitecore.DependencyInjection;

namespace SitecoreFindADoctorSearch.Respository
{
    public class ExecuteSearch
    {
        public QueryResponse DoSearch(PhysicianQuery query)
        {
            //Filters
            FiltersFacets filtersFacets = new FiltersFacets();

            //Create an object to hold results
            SolrQueryResults<Physician> solrResults;
            QueryResponse queryResponse = new QueryResponse();

            //Echo back the original query 
            queryResponse.OriginalQuery = query;


            //Get a connection
            ISolrOperations<Physician> solr = ServiceLocator.Current.GetInstance<ISolrOperations<Physician>>();

     

            //Set Options
            QueryOptions queryOptions = new QueryOptions
            {
                Rows = query.Rows,
                Start = query.Start,
                FilterQueries = filtersFacets.BuildFilterQueries(query),
                Facet = filtersFacets.BuildFacets(),
            };

            // test location
            //query.Location = "29.8039999,-95.5581533";

            // test distance
            query.Distance = "40";

            // Sort by distance or score/random
            if (query.Location != "" && query.Location.IndexOf(',') > -1 && query.Distance != "")
            {
                queryOptions.OrderBy = new[] { new SortOrder("geodist()", Order.ASC) };
                queryOptions.ExtraParams = new Dictionary<string, string>
                    {
                        { "sfield", "location" },
                        { "pt", query.Location },
                        // return distance in miles
                        { "fl", "distance:product(geodist(),0.62137)"},
                    };
            }
            else if (query.Location != "" && query.Location.IndexOf(',') > -1)
            {
                queryOptions.OrderBy = new[] { new SortOrder("geodist()", Order.ASC) };
                queryOptions.ExtraParams = new Dictionary<string, string>
                    {
                        { "sfield", "location" },
                        { "pt", query.Location },
                        // return distance in miles
                        { "fl", "distance:product(geodist(),0.62137)"},
                    };
            }
            else
            {
                int rInt;
                if (query.RandomInt != 0)
                {
                    rInt = query.RandomInt;
                }
                else
                {
                    Random r = new Random();
                    rInt = r.Next(0, 9999);
                }
                queryOptions.OrderBy = new[] {
                        new SortOrder("score", Order.DESC),
                        new SortOrder("affinityscore_tl", Order.DESC),
                        new SortOrder("random_" + rInt, Order.DESC)
                        // Affinity Random Sort if needed by business owners - comment above and use below
                        //new SortOrder("sub(sum(.1,log(random_" + rInt + ")),floor(log(random_" + rInt + ")))", Order.DESC)
                    };
            }

            //Execute the query
            ISolrQuery solrQuery = new SolrQuery(query.Query);

            solrResults = solr.Query(solrQuery, queryOptions);

            //Set response
            ResponseExtraction extractResponse = new ResponseExtraction();

            extractResponse.SetHeader(queryResponse, solrResults);
            extractResponse.SetBody(queryResponse, solrResults);
            extractResponse.SetFacets(queryResponse, solrResults);

            //Return response;
            return queryResponse;
        }

        public QueryResponse DoTypeahead(PhysicianQuery query)
        {
            //Create an object to hold results
            SolrQueryResults<Physician> solrResults;
            QueryResponse queryResponse = new QueryResponse();

            //Echo back the original query 
            queryResponse.OriginalQuery = query;

            //Get a connection (swapped to executor for support for different request handler)
            var executor = ServiceLocator.Current.GetInstance<ISolrQueryExecuter<Physician>>() as SolrQueryExecuter<Physician>;

            //Use suggest handler
            executor.Handler = "/suggest";

            //Set Options
            QueryOptions queryOptions = new QueryOptions
            {
                Rows = query.Rows,
                Start = query.Start,
            };

            //Execute the query
            ISolrQuery solrQuery = new SolrQuery(query.Query);

            //swap to executor
            solrResults = executor.Execute(solrQuery, queryOptions);

            //Set response
            ResponseExtraction extractResponse = new ResponseExtraction();

            extractResponse.SetHeader(queryResponse, solrResults);
            extractResponse.SetBody(queryResponse, solrResults);

            //Return response;
            return queryResponse;
        }

        public QueryResponse DoDirectoryList(PhysicianQuery query)
        {
            //Filters
            FiltersFacets filtersFacets = new FiltersFacets();

            //Create an object to hold results
            SolrQueryResults<Physician> solrResults;
            QueryResponse queryResponse = new QueryResponse();

            //Echo back the original query 
            queryResponse.OriginalQuery = query;

            //Get a connection (swapped to executor for support for different request handler)
            var executor = ServiceLocator.Current.GetInstance<ISolrQueryExecuter<Physician>>() as SolrQueryExecuter<Physician>;

            //Use suggest handler
            executor.Handler = "/atoz";

            //Set Options
            QueryOptions queryOptions = new QueryOptions
            {
                Rows = query.Rows,
                Start = query.Start,
                FilterQueries = filtersFacets.BuildFilterQueries(query),
            };

            //Execute the query
            ISolrQuery solrQuery = new SolrQuery(query.Query);

            //swap to executor
            solrResults = executor.Execute(solrQuery, queryOptions);

            //Set response
            ResponseExtraction extractResponse = new ResponseExtraction();

            extractResponse.SetHeader(queryResponse, solrResults);
            extractResponse.SetBody(queryResponse, solrResults);

            //Return response;
            return queryResponse;
        }
    }
}