﻿using SitecoreFindADoctorSearch.Models;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Respository
{
    internal class FiltersFacets
    {
        // Filters
        internal ICollection<ISolrQuery> BuildFilterQueries(PhysicianQuery query)
        {
            ICollection<ISolrQuery> filters = new List<ISolrQuery>();
            List<SolrQueryByField> filtersSpecialty = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersGender = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersInterest = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersAffiliation = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersInsurance = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersScheduleNow = new List<SolrQueryByField>();
            List<SolrQueryByField> filtersLastName = new List<SolrQueryByField>();

            // get filters
            foreach (string specialty in query.SpecialtyFilter)
            {
                filtersSpecialty.Add(new SolrQueryByField("primaryspecialty_s", specialty));
            }

            foreach (string gender in query.GenderFilter)
            {
                filtersGender.Add(new SolrQueryByField("gender_s", gender));
            }

            foreach (string interest in query.InterestFilter)
            {
                filtersInterest.Add(new SolrQueryByField("clinicalinterests_pipe", interest));
            }

            foreach (string affiliation in query.AffiliationFilter)
            {
                filtersAffiliation.Add(new SolrQueryByField("facilities_pipe", affiliation));
            }

            foreach (string insurance in query.InsuranceFilter)
            {
                filtersInsurance.Add(new SolrQueryByField("insuranceplans_pipe", insurance));
            }

            foreach (string schedulenow in query.ScheduleNowFilter)
            {
                filtersScheduleNow.Add(new SolrQueryByField("schedulenowurl_s", schedulenow));
            }

            foreach (string lastname in query.LastNameFilter)
            {
                filtersLastName.Add(new SolrQueryByField("lastname_fl", lastname));
            }

            // set filters
            if (filtersSpecialty.Count > 0)
            {
                filters.Add(new LocalParams { { "tag", "st" } } + new SolrMultipleCriteriaQuery(filtersSpecialty, "OR"));
            }

            if (filtersGender.Count > 0)
            {
                filters.Add(new LocalParams { { "tag", "gt" } } + new SolrMultipleCriteriaQuery(filtersGender, "OR"));
            }

            if (filtersInterest.Count > 0)
            {
                filters.Add(new LocalParams { { "tag", "ct" } } + new SolrMultipleCriteriaQuery(filtersInterest, "OR"));
            }

            if (filtersAffiliation.Count > 0)
            {
                filters.Add(new LocalParams { { "tag", "ht" } } + new SolrMultipleCriteriaQuery(filtersAffiliation, "OR"));
            }

            if (filtersInsurance.Count > 0)
            {
                filters.Add(new LocalParams { { "tag", "it" } } + new SolrMultipleCriteriaQuery(filtersInsurance, "OR"));
            }

            if (filtersScheduleNow.Count > 0)
            {
                filters.Add(new SolrMultipleCriteriaQuery(filtersScheduleNow, "OR"));
            }

            if (filtersLastName.Count > 0)
            {
                filters.Add(new SolrMultipleCriteriaQuery(filtersLastName, "OR"));
            }

            return filters;
        }

        // Facets
        internal FacetParameters BuildFacets()
        {
            return new FacetParameters
            {
                Queries = new List<ISolrFacetQuery>
                {
                    new SolrFacetFieldQuery(new LocalParams { { "ex", "st" } } + "primaryspecialty_s") { MinCount = 1 },
                    new SolrFacetFieldQuery(new LocalParams { { "ex", "gt" } } + "gender_s") { MinCount = 1 },
                    new SolrFacetFieldQuery(new LocalParams { { "ex", "ct" } } + "clinicalinterests_pipe") { MinCount = 1 },
                    new SolrFacetFieldQuery(new LocalParams { { "ex", "ht" } } + "facilities_pipe") { MinCount = 1 },
                    new SolrFacetFieldQuery(new LocalParams { { "ex", "it" } } + "insuranceplans_pipe") { MinCount = 1 }
                }
            };
        }
    }
}