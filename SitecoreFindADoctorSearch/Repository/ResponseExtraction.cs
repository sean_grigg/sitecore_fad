﻿using SitecoreFindADoctorSearch.Models;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Respository
{
    public class ResponseExtraction
    {
        //Extract parts of the SolrNet response and set them in QueryResponse class
        internal void SetHeader(QueryResponse queryResponse, SolrQueryResults<Physician> solrResults)
        {
            queryResponse.QueryTime = solrResults.Header.QTime;
            queryResponse.Status = solrResults.Header.Status;
            queryResponse.TotalHits = solrResults.NumFound;
        }

        internal void SetBody(QueryResponse queryResponse, SolrQueryResults<Physician> solrResults)
        {
            queryResponse.Results = (List<Physician>)solrResults;
        }

        internal void SetFacets(QueryResponse queryResponse, SolrQueryResults<Physician> solrResults)
        {
            // Specialties
            if (solrResults.FacetFields.ContainsKey("primaryspecialty_s"))
            {
                queryResponse.SpecialtyFacet = solrResults.FacetFields["primaryspecialty_s"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }

            // Gender
            if (solrResults.FacetFields.ContainsKey("gender_s"))
            {
                queryResponse.GenderFacet = solrResults.FacetFields["gender_s"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }

            // Interests
            if (solrResults.FacetFields.ContainsKey("clinicalinterests_pipe"))
            {
                queryResponse.InterestFacet = solrResults.FacetFields["clinicalinterests_pipe"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }

            // Affiliation
            if (solrResults.FacetFields.ContainsKey("facilities_pipe"))
            {
                queryResponse.AffiliationFacet = solrResults.FacetFields["facilities_pipe"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }

            // Insurance
            if (solrResults.FacetFields.ContainsKey("insuranceplans_pipe"))
            {
                queryResponse.InsuranceFacet = solrResults.FacetFields["insuranceplans_pipe"].Select(facet => new KeyValuePair<string, int>(facet.Key, facet.Value)).ToList();
            }
        }
    }
}