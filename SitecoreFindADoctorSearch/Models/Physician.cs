﻿using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Models
{
    public class Physician
    {
        [SolrUniqueKey("firstname_s")]
        public string FirstName { get; set; }

        [SolrField("lastname_s")]
        public string LastName { get; set; }

        [SolrField("degree_s")]
        public string Degree { get; set; }

        [SolrField("gender_s")]
        public string Gender { get; set; }

        [SolrField("primaryspecialty_s")]
        public string Specialty { get; set; }

        [SolrField("_fullpath")]
        public string FullPath { get; set; }

        [SolrField("_uniqueid")]
        public string ItemID { get; set; }

        [SolrField("msoid_s")]
        public string MSOID { get; set; }

        [SolrField("schedulenowurl_s")]
       
        public string ScheduleNow { get; set; }

        [SolrField("ismhmg_tl")]
        public long IsMHMG { get; set; }

        [SolrField("ismhmgpediatrics_tl")]
        public long IsMHMGPediatrics { get; set; }

        [SolrField("isacceptingpatients_tl")]
        public long IsAccepting { get; set; }

        [SolrField("excludefromsearch_tl")]
        public long ExcludeFromSearch { get; set; }

        [SolrField("location")]
        public string PrimaryLatLong { get; set; }

        // testing distance
        [SolrField("distance")]
        public float Distance { get; set; }
    }
}