﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Models
{
    public class Search
    {
        public Search()
        {
            LocationGiven = false;
            Physicians = new List<Physician>();
            currentFacets = new CurrentFacets();
            returnedFacets = new ReturnedFacets();
        }

        public List<Physician> Physicians { get; set; }

        public CurrentFacets currentFacets { get; set; }

        public ReturnedFacets returnedFacets { get; set; }

        public bool LocationGiven { get; set; }

        public int TotalDocs { get; set; }

        public int Start { get; set; }

        public int Rows { get; set; }

    }
}