﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Models
{
    public class QueryResponse
    {
        public QueryResponse()
        {
            //Initialize properties
            SpecialtyFacet = new List<KeyValuePair<string, int>>();
            GenderFacet = new List<KeyValuePair<string, int>>();
            InterestFacet = new List<KeyValuePair<string, int>>();
            AffiliationFacet = new List<KeyValuePair<string, int>>();
            InsuranceFacet = new List<KeyValuePair<string, int>>();
        }

        //Expose properties that will be returned to from solr
        public List<Physician> Results { get; set; }

        public int TotalHits { get; set; }

        public int QueryTime { get; set; }

        public int Status { get; set; }

        public PhysicianQuery OriginalQuery { get; set; }

        public List<KeyValuePair<string, int>> SpecialtyFacet { get; set; }

        public List<KeyValuePair<string, int>> GenderFacet { get; set; }

        public List<KeyValuePair<string, int>> InterestFacet { get; set; }

        public List<KeyValuePair<string, int>> AffiliationFacet { get; set; }

        public List<KeyValuePair<string, int>> InsuranceFacet { get; set; }
    }
}