﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Models
{
    public class CurrentFacets
    {
        public CurrentFacets()
        {
            SpecialtyFacet = new List<string>();
            GenderFacet = new List<string>();
            InterestFacet = new List<string>();
            AffiliationFacet = new List<string>();
            InsuranceFacet = new List<string>();
        }

        public List<string> SpecialtyFacet { get; set; }

        public List<string> GenderFacet { get; set; }

        public List<string> InterestFacet { get; set; }
  
        public List<string> AffiliationFacet { get; set; }

        public List<string> InsuranceFacet { get; set; }
    }
}