﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Models
{
    public class PhysicianQuery
    {
        public PhysicianQuery()
        {
            Rows = 10;
            Start = 0;
            Location = "";
            Distance = "";
            RandomInt = 0;
            SpecialtyFilter = new List<string>();
            GenderFilter = new List<string>();
            InterestFilter = new List<string>();
            AffiliationFilter = new List<string>();
            InsuranceFilter = new List<string>();
            ScheduleNowFilter = new List<string>();
            LastNameFilter = new List<string>();
        }
        //Query object that holds parameters sent to solr
        public string Query { get; set; }

        public int Start { get; set; }

        public int Rows { get; set; }

        public int RandomInt { get; set; }

        public string Location { get; set; }

        public string Distance { get; set; }

        public List<string> SpecialtyFilter { get; set; }

        public List<string> GenderFilter { get; set; }

        public List<string> InterestFilter { get; set; }

        public List<string> AffiliationFilter { get; set; }

        public List<string> InsuranceFilter { get; set; }

        public List<string> ScheduleNowFilter { get; set; }

        public List<string> LastNameFilter { get; set; }
    }
}