﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecoreFindADoctorSearch.Models
{
    public class ReturnedFacets
    {
        public ReturnedFacets()
        {
            SpecialtyFacet = new List<KeyValuePair<string, int>>();
            GenderFacet = new List<KeyValuePair<string, int>>();
            InterestFacet = new List<KeyValuePair<string, int>>();
            AffiliationFacet = new List<KeyValuePair<string, int>>();
            InsuranceFacet = new List<KeyValuePair<string, int>>();
        }

        public List<KeyValuePair<string, int>> SpecialtyFacet { get; set; }

        public List<KeyValuePair<string, int>> GenderFacet { get; set; }

        public List<KeyValuePair<string, int>> InterestFacet { get; set; }

        public List<KeyValuePair<string, int>> AffiliationFacet { get; set; }

        public List<KeyValuePair<string, int>> InsuranceFacet { get; set; }
    }
}