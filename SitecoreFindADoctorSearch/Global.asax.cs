﻿using SitecoreFindADoctorSearch.Models;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Sitecore.Web;

namespace SitecoreFindADoctorSearch
{
    public class MvcApplication : Sitecore.Web.Application //System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //RouteConfig.RegisterRoutes(RouteTable.Routes);

            Startup.Init<Physician>("http://mcedwwebdev01:8983/solr/sitecore_physician_index");
        }
    }
}
