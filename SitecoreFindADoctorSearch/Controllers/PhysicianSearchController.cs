﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SitecoreFindADoctorSearch.Models;
using System.Web.Mvc;

namespace SitecoreFindADoctorSearch.Controllers
{
    public class PhysicianSearchController : Controller
    {
        // GET: PhysicianSearch
        public ActionResult PhysicianDetailViewLayout()
        {
            return View();
        }

        public ActionResult PhysicianSearchResultsViewLayout()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getViews()
        {
            // get params
            string q = Request.Params.Get("query");
            string specialties = Request.Params.Get("specialty");
            string genders = Request.Params.Get("gender");
            string interests = Request.Params.Get("interest");
            string affiliations = Request.Params.Get("affiliation");
            string insurances = Request.Params.Get("insurance");
            string location = Request.Params.Get("latlng");
            string schedulenow = Request.Params.Get("schedulenow");
            int start;
            int.TryParse(Request.Params.Get("start"), out start);
            int random;
            int.TryParse(Request.Params.Get("random"), out random);

            // build query
            Respository.ExecuteSearch searchLibrary = new Respository.ExecuteSearch();
            PhysicianQuery query = new PhysicianQuery();
            query.Query = q;
            query.Start = start;
            query.RandomInt = random;

            // get location
            if (location != null && location != "")
            {
                query.Location = location;
            }
            
            // add filters
            if (specialties != null && specialties != "")
            {
                List<string> s = specialties.Split(',').ToList();
                foreach (string item in s)
                {
                    query.SpecialtyFilter.Add(item);
                }
            }
            else { specialties = ""; }

            if (genders != null && genders != "")
            {
                List<string> g = genders.Split(',').ToList();
                foreach (string item in g)
                {
                    query.GenderFilter.Add(item);
                }
            }
            else { genders = ""; }

            if (interests != null && interests != "")
            {
                List<string> i = interests.Split(',').ToList();
                foreach (string item in i)
                {
                    query.InterestFilter.Add(item);
                }
            }
            else { interests = ""; }

            if (affiliations != null && affiliations != "")
            {
                List<string> i = affiliations.Split(',').ToList();
                foreach (string item in i)
                {
                    query.AffiliationFilter.Add(item);
                }
            }
            else { affiliations = ""; }

            if (insurances != null && insurances != "")
            {
                List<string> i = insurances.Split(',').ToList();
                foreach (string item in i)
                {
                    query.InsuranceFilter.Add(item);
                }
            }
            else { insurances = ""; }

            if (schedulenow == "true")
            {
                query.ScheduleNowFilter.Add("[* TO *]");
            }
            

            // query solr
            QueryResponse response = searchLibrary.DoSearch(query);

            // load model
            Search model = new Search();
            model.Physicians = response.Results;
            model.currentFacets.SpecialtyFacet = specialties.Split(',').ToList();
            model.currentFacets.GenderFacet = genders.Split(',').ToList();
            model.currentFacets.InterestFacet = interests.Split(',').ToList();
            model.currentFacets.AffiliationFacet = affiliations.Split(',').ToList();
            model.currentFacets.InsuranceFacet = insurances.Split(',').ToList();
            model.returnedFacets.SpecialtyFacet = response.SpecialtyFacet;
            model.returnedFacets.GenderFacet = response.GenderFacet;
            model.returnedFacets.InterestFacet = response.InterestFacet;
            model.returnedFacets.AffiliationFacet = response.AffiliationFacet;
            model.returnedFacets.InsuranceFacet = response.InsuranceFacet;
            model.Start = query.Start;
            model.Rows = query.Rows;
            model.TotalDocs = response.TotalHits;
            model.LocationGiven = (query.Location != "") ? true : false;

            // return partials as json
            var searchResultsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_SearchResults", model);
            var mobileSearchResultsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_MobileSearchResults", model);
            var specialtiesFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_SpecialtiesFacets", model);
            var genderFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_GenderFacets", model);
            var interestFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_InterestFacets", model);
            var affiliationFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_AffiliationFacets", model);
            var insuranceFacetsView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_InsuranceFacets", model);
            var paginationView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_Pagination", model);
            var mobilePaginationView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_MobilePagination", model);
            var numberFoundView = Helper.RenderViewToString.RenderView(this.ControllerContext, "_NumberFound", model);
            var json = Json(new { searchResultsView, mobileSearchResultsView, mobilePaginationView, specialtiesFacetsView, genderFacetsView, interestFacetsView, affiliationFacetsView, insuranceFacetsView, paginationView, numberFoundView });
            return json;
        }

        [HttpPost]
        public JsonResult getTypeahead()
        {
            // get params
            string q = Request.Params.Get("query");

            // build query
            Respository.ExecuteSearch searchLibrary = new Respository.ExecuteSearch();
            PhysicianQuery query = new PhysicianQuery();
            query.Query = q;

            // query solr
            QueryResponse response = searchLibrary.DoTypeahead(query);

            // load model
            Search model = new Search();
            model.Physicians = response.Results;

            List<Typeahead> ta = new List<Typeahead>();
            foreach (var item in model.Physicians)
            {
                Typeahead test = new Typeahead();
                test.label = item.FirstName + " " + item.LastName;
                test.value = item.FirstName + " " + item.LastName;
                test.link = item.FullPath;
                test.type = "physician";
                ta.Add(test);
            }

            return Json(ta);
        }

        [HttpPost]
        public JsonResult getFacets()
        {

            // build query
            Respository.ExecuteSearch searchLibrary = new Respository.ExecuteSearch();
            PhysicianQuery query = new PhysicianQuery();
            query.Query = "*:*";

            // query solr
            QueryResponse response = searchLibrary.DoSearch(query);

            // load model
            //Search model = new Search();
            //model.returnedFacets.SpecialtyFacet = response.SpecialtyFacet;
            //model.returnedFacets.GenderFacet = response.GenderFacet;

            List<KeyValuePair<string, List<Typeahead>>> facets = new List<KeyValuePair<string, List<Typeahead>>>();

            List<Typeahead> specialty = new List<Typeahead>();
            foreach (var item in response.SpecialtyFacet)
            {
                Typeahead ta = new Typeahead();
                ta.label = item.Key;
                ta.value = item.Key;
                ta.link = "";
                ta.type = "specialty";
                specialty.Add(ta);
            }
            facets.Add(new KeyValuePair<string, List<Typeahead>>("Specialty", specialty));

            return Json(facets);
        }
    }
}