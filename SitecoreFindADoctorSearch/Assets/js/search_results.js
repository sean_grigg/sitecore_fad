﻿var map;
var mobileMap;
var origFilterText = ["Specialty", "Clinical Interest", "Gender", "Insurance", "Hospital Affiliation"];

function initPopovers(resp) {
    //SPECIALTY FACETS
    //Check if popover already exists, if not (1st search), initialize all necessary options
    if (!$('.filter-list-item.specialty').data('bs.popover')) {
        $('.filter-list-item.specialty').popover({
            content: function () { return resp.specialtiesFacetsView; },
            trigger: 'manual',
            html: true
        });
    } else {
        //If popover exists (2nd+ search), only need to update filter content
        $('.filter-list-item.specialty').data('bs.popover').options
            .content = resp.specialtiesFacetsView;
    }

    //CLINICAL INTEREST FACETS
    if (!$('.filter-list-item.clinical-interest').data('bs.popover')) {
        $('.filter-list-item.clinical-interest').popover({
            content: function () { return resp.interestFacetsView; },
            trigger: 'manual',
            html: true
        });
    } else {
        $('.filter-list-item.clinical-interest').data('bs.popover').options
            .content = resp.interestFacetsView;
    }

    //GENDER FACETS
    if (!$('.filter-list-item.gender').data('bs.popover')) {
        $('.filter-list-item.gender').popover({
            content: function () { return resp.genderFacetsView; },
            trigger: 'manual',
            html: true
        });
    } else {
        $('.filter-list-item.gender').data('bs.popover').options
            .content = resp.genderFacetsView;
    }

    //INSURANCE FACETS
    if (!$('.filter-list-item.insurance').data('bs.popover')) {
        $('.filter-list-item.insurance').popover({
            content: function () { return resp.insuranceFacetsView; },
            trigger: 'manual',
            html: true
        });
    } else {
        $('.filter-list-item.insurance').data('bs.popover').options
            .content = resp.insuranceFacetsView;
    } 

    //HOSPITAL AFFILIATION FACETS
    if (!$('.filter-list-item.hospital-affiliation').data('bs.popover')) {
        $('.filter-list-item.hospital-affiliation').popover({
            content: function () { return resp.affiliationFacetsView; },
            trigger: 'manual',
            html: true
        });
    } else {
        $('.filter-list-item.hospital-affiliation').data('bs.popover').options
            .content = resp.affiliationFacetsView;
    }
}

//Function to uncheck all filters from the currently open filter popover - User clicked "Clear"
function clearVisibleFilters(item) {
    item.closest('.popover-content').find('input').prop('checked', '');
    item.closest('.popover-content').find('input[type="radio"][value=""]').prop('checked', 'checked');
    querySolr();
}

//Deactivate all popover filters
function closeAllPopovers() {
    $('.filter-list-item').popover('hide');
    $('.filter-block').each(function () {
        $(this).removeClass('showing');
        checkActive($(this));
    });  
}

//Deactivate all popover filters EXCEPT the one that was just activated
function closeAllOtherPopovers() {
    $('.filter-list-item').each(function () {
        var $filterBlock = $(this).find('.filter-block');
        if (!$filterBlock.hasClass('showing')) {
            $(this).popover('hide');
            checkActive($filterBlock);
        }
    })
}

//Fixes bootstrap popover bug requiring two clicks to show after closeAllPopovers() 
$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});

//Function to update filter button text given array of user filter selections
//checked = array of selected filters, $Btn = filter button to update, origText = fallback text if filters are cleared
function updateBtnText(checked, $Btn, origText) {
    if (checked.length == 1) {
        if (checked[0] != '') {
            if (checked[0].length <= 20) {
                $Btn.text(checked[0]);
            } else {
                $Btn.text(checked[0].substring(0, 20) + '...');
            }
            $Btn.addClass('active');
        } else {
            $Btn.text(origText);
        }
    } else if (checked.length > 1) {
        $Btn.text(checked.length + " Selected");
        $Btn.addClass('active');
    }
}

//Pass in $('.filter-block') item to check if it should remain active given current state of filters 
function checkActive($filterBlock) {
    var isOriginalText = false;
    for (var i = 0; i < origFilterText.length; i++) {
        if ($filterBlock.text().trim() == origFilterText[i]) {
            isOriginalText = true;
        }
    }
    if (isOriginalText) {
        $filterBlock.removeClass('active');
    }
}


//Filters down facets when there are 25+ in a facet list as user types in filter search bar
function filterFacets($item) {
    var valThis = $item.val().toLowerCase(),
      length = valThis.length;

    $item.parents('.popover-content').find('.filter-input-row').each(function () {
        var text = $(this).find('.facet-name').text().trim(),
          textL = text.toLowerCase(),
          htmlR = ' <b>' + text.substr(0, length) + '</b>' + text.substr(length);
        if (textL.indexOf(valThis) == 0) {
            $(this).show();
            $(this).find('.facet-name').html(htmlR);
        } else {
            $(this).hide();
        }
    });

}

function checkContainer() {
    var w = window.innerWidth;
    //console.log(w);
    if (w < 900) {
        if ($('#advanced-search-container').hasClass('container-fluid')) {
            $('#advanced-search-container').removeClass('container-fluid').addClass('container');
        }
    } else {
        if ($('#advanced-search-container').hasClass('container')) {
            $('#advanced-search-container').removeClass('container').addClass('container');
        }
    }
}

//Pagination
function next(start) {
    querySolr(start);
}

function previous(start) {
    querySolr(start);
}



function initTab() {
    $("#findDoctorTab li a").click(function () {
        $("#findDoctorTab li a").each(function (index) {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        var content = $(this).data('id');
        $('.tab-content .active').removeClass('active');
        $('#' + content).addClass('active');

        updateMapContainerHeight();
        google.maps.event.trigger(mobileMap, "resize");
    });
}

function updateMapContainerHeight() {
    var viewportWidth = $(window).width();

    if (viewportWidth <= 767) {
        var viewportHeight = $(window).height();
        var mobileMapPosition = $('#mobileMap').offset();
        var mobileMapHeight = mobileMapPosition.top;
        var dynamicHeight = viewportHeight - mobileMapHeight;
        $('.search-results-map-block').css('height', dynamicHeight);
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'),
    {
        center: { lat: 29.781313, lng: -95.546905 },
        zoom: 15
    });

    mobileMap = new google.maps.Map(document.getElementById('mobileMap'),
    {
        center: { lat: 29.781313, lng: -95.546905 },
        zoom: 15
    });
}

//Initialize Advanced Search Popover
function initAdvancedSearchUI() {
    //Populate dropdown options
    var specialties = '';
    for (var i in initSpecialties) {
        specialties += '<option value="' + initSpecialties[i].label + '">' + initSpecialties[i].label + '</option>';
    }
    $('#advanced-specialty-dropdown').html(specialties);

    //Initialize the advanced search schedule now slide toggle
    $('.schedule-now-wrap .toggle').toggles({
        text: {
            on: '', // text for the ON position
            off: '' // and off
        },
        height: 25,
        width: 45
    });

    $('.advanced-search-block').popover({
        container: '#advanced-search-container',
        content: function () { return $('#advanced-search-ui').html(); },
        trigger: 'click',
        template: '<div class="popover advanced-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        html: true
    });


    //Open/Close multiselect dropdown when clicking on icon
    $('.advanced-search-block').on('shown.bs.popover', function () {
        $('.popover .chosen-select').chosen({
            width: '100%'
        });
        $('.popover .chosen-choices input').attr('style', 'width:100%;');
        $('.popover .advanced-search-wrap').show();

        //Open/Close multiselect dropdown when clicking on the input field
        $('.chosen-select').on('chosen:showing_dropdown', function () {
            $(this).closest('.advanced-dropdown-wrap').find('i')
            .removeClass('fa-chevron-down').addClass('fa-chevron-up');
        });
        $('.chosen-select').on('chosen:hiding_dropdown', function () {
            $(this).closest('.advanced-dropdown-wrap').find('i')
            .removeClass('fa-chevron-up').addClass('fa-chevron-down');
        });

        $('.advanced-dropdown-wrap i').click(function () {
            if ($(this).hasClass('fa-chevron-down')) {
                $(this).closest('.advanced-dropdown-wrap').find('.chosen-select').trigger('chosen:open');
            } else {
                $(this).closest('.advanced-dropdown-wrap').find('.chosen-select').trigger('chosen:close');
            }
        });
    });


}

//Document Ready
$(document).ready(function () {
    initTab();

    //Deactivates filter popovers when clicking outside them
    $('body').on('click', function (e) {
        var closedPopover = false;
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
                closedPopover = true;
            }
        });
        if (closedPopover) {
            $('.filter-block').each(function () {
                $(this).removeClass('showing');
                checkActive($(this));
            });
        }
    });

    //Toggles Showing the filter popup on click
    $('.filter-list-item').click(function (e) {
        var isShowing = false;
        var $filterBlock = $(this).find('.filter-block');
        if ($filterBlock.hasClass('showing')) {
            isShowing = true;
        }
        $('.filter-block').removeClass('showing');
        if (!isShowing) {
            $filterBlock.addClass('showing active');
            closeAllOtherPopovers();
            $(this).popover('show');
        } else {
            $(this).popover('hide');
            checkActive($filterBlock);
        }
        e.stopPropagation();
    });

    //Toggle the advanced search icon up/down
    $('.advanced-search-block').click(function () {
        $(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
    });

    //Initialize slide toggle
    $('.toggle').toggles({
        text: {
            on: '', // text for the ON position
            off: '' // and off
        },
        height: 15
    });
});