﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using SitecoreFindADoctorSearch.Models;
using SolrNet.Commands.Parameters;
using SolrNet.Impl;
using SolrNet;

namespace SitecoreFindADoctorSearch
{
    public class ISolrOperationsConfigurator : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<ISolrOperations<Physician>>();
        }
    }
}