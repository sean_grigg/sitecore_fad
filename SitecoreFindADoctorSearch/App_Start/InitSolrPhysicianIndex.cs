﻿using SitecoreFindADoctorSearch.Models;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using Sitecore;
using Sitecore.Pipelines;

namespace SitecoreFindADoctorSearch.Pipelines.Initialize
{
    public class InitSolrPhysicianIndex
    {
        public virtual void Process(PipelineArgs args)
        {
            InitPhysicianSolrConnection(args);
        }

        private void InitPhysicianSolrConnection(PipelineArgs args)
        {
            Startup.Init<Physician>("http://mcedwwebdev01:8983/solr/sitecore_physician_index");
        }
    }
}